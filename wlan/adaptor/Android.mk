LOCAL_PATH := $(call my-dir)

ifeq ($(MTK_WLAN_SUPPORT), yes)
ifneq (true,$(strip $(TARGET_NO_KERNEL)))

LOCAL_KERNEL_MODULE := wmt_chrdev_wifi
EXTRA_KERNEL_MODULE_PATH_$(LOCAL_KERNEL_MODULE) := $(LOCAL_PATH)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.wlan_drv.rc
LOCAL_SRC_FILES            := init.wlan_drv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

endif
endif
