LOCAL_PATH := $(call my-dir)

ifneq ($(filter yes,$(sort $(MTK_WLAN_SUPPORT) $(MTK_BT_SUPPORT) $(MTK_GPS_SUPPORT) $(MTK_FM_SUPPORT))),)

ifneq (true,$(strip $(TARGET_NO_KERNEL)))
ifneq ($(filter yes,$(MTK_COMBO_SUPPORT)),)

LOCAL_KERNEL_MODULE := wmt_drv
EXTRA_KERNEL_MODULE_PATH_$(LOCAL_KERNEL_MODULE) := $(LOCAL_PATH)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.wmt_drv.rc
LOCAL_SRC_FILES            := init.wmt_drv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

endif
endif

endif
